document.addEventListener('DOMContentLoaded', function () {
  function onDraw () {
    const ctx = document.querySelector('#canvas').getContext('2d')
    if (!ctx) {
      throw new Error // => выбросить ошибку, если контекст не был взят
    } else {
      
      // DRAW RECTANGLE
      ctx.beginPath()
      ctx.rect(75,100,250,150)
      ctx.closePath()

      ctx.fillStyle = '#51dcff'
      ctx.strokeStyle = '#666'
      ctx.lineWidth = 4

      ctx.fill()
      ctx.stroke()
    }
  }

  function onFillRect () {
    const ctx = document.querySelector('#fillRect').getContext('2d')
    if (!ctx) {
      throw Error // => выбросить ошибку, если контекст не был взят
    } else {
      ctx.beginPath()
      ctx.fillStyle = 'firebrick'
      ctx.fillRect(50,50,300,200) // => shorcut of commands rect()+fill()
      ctx.closePath()
    }
  }

  function onStrokeRect () {
    const ctx = document.querySelector('#strokeRect').getContext('2d')
    if (!ctx) {
      throw Error // => выбросить ошибку, если контекст не был взят
    } else {
      ctx.beginPath()
      ctx.strokeStyle = 'firebrick'
      ctx.lineWidth = 3
      ctx.strokeRect(50,50,300,200) // => shorcut of commands rect()+stroke()
      ctx.closePath()
    }
  }





  onDraw()
  onFillRect()
  onStrokeRect()
})

