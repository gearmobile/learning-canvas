document.addEventListener('DOMContentLoaded', function () {

  const numberOfCircles = 40
  const radiusMin = 15
  const radiusMax = 150

  function getRadians (value) {
    if (value === null || value === undefined) {
      throw new Error
    } else {
      return ( Math.PI / 180 ) * value
    }
  }

  function onDrawCircle (radius, ctx) {

      // get random position of circle
      const x = Math.round(Math.random() * ctx.canvas.width) // => get x position in range of canvas width
      const y = Math.round(Math.random() * ctx.canvas.height) // => get y position in range of canvas height
      
      // draw circle
      ctx.beginPath()
      ctx.arc(x, y, radius, getRadians(0), getRadians(360), true)
      ctx.fillStyle = 'rgba(41, 170, 255, .1)'
      ctx.fill()
      ctx.closePath()
  }

  function onDraw (context) {
    for ( let i = 0; i < numberOfCircles; i += 1 ) {
      const radius = Math.round( radiusMin + Math.random() * radiusMax )
      onDrawCircle(radius, context)
    }  
  }

  function onRender (radius) {
    const ctx = document.querySelector('#canvas').getContext('2d')
    if (!ctx) {
      throw new Error // => выбросить ошибку, если контекст не был взят
    } else {
      onDraw(ctx)
    }
  }

  onRender()

})

