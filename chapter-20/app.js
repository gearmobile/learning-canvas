window.addEventListener('load', function () {

  // => helper function to return exact position some element
  function getElementPosition (el) {
    
    // => init variables
    let xPos = null
    let yPos = null

    if (el) {
      if (el.tagName === 'BODY') {
        const xScroll = el.scrollLeft || document.documentElement.scrollLeft
        const yScroll = el.scrollTop || document.documentElement.scrollTop
        xPos += (el.offsetLeft - xScroll + el.clientLeft)
        yPos += (el.offsetTop - yScroll + el.clientTop) 
      } else {
        xPos += (el.offsetLeft - el.scrollLeft + el.clientLeft)
        yPos += (el.offsetTop - el.scrollTop + el.clientTop)
      }
      el = el.offsetParent
      return { xPos, yPos }
    }
  }

  // => get mouse cursor position
  function getMouseCursorPosition (event) {
    let xPos = event.clientX - getElementPosition(ctx.canvas).xPos // => get current x value cursor mouse inside canvas
    let yPos = event.clientY - getElementPosition(ctx.canvas).yPos // => get current y value cursor mouse inside canvas
    return { xPos, yPos }
  }

  const ctx = document.querySelector('#canvas').getContext('2d')

  const mouse = {
    xPos: 200
  }

  const image = {
    width: 875,
    height: 83,
    src: './src/sprite.png'
  }

  if (!ctx) {
    throw new Error
  }

  function ctxInit () {
    ctx.strokeStyle = 'firebrick'
  }

  // => draw scale
  function drawScale () {
    const scale = new Image()
    scale.src = image.src
    scale.addEventListener('load', () => { ctx.drawImage(scale, 0, ctx.canvas.height - image.height, image.width, image.height) }, false)
  }

  // => draw line
  function drawLine () {
    ctx.beginPath()
    ctx.moveTo(0, ctx.canvas.height - image.height)
    ctx.quadraticCurveTo(mouse.xPos / 2, ctx.canvas.height - (mouse.xPos * 3)/2, mouse.xPos, ctx.canvas.height - 83);
    ctx.stroke()
    ctx.closePath()
  }

  ctx.canvas.addEventListener('mousedown', function (event) {
    console.log(getMouseCursorPosition(event))
  }, false)

  ctxInit()
  drawScale()
  drawLine()

}, false)


