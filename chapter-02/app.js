
function quadraticBezier () {
  
  const ctx = document.querySelector('#quadraticBezier').getContext('2d')
  
  if (!ctx) {
    throw new Error // => выбросить ошибку, если контекст не был взят
  } else {
    ctx.strokeStyle = 'green'
    ctx.lineWidth = 3
  
    ctx.moveTo(20, 20)
    ctx.quadraticCurveTo(300,10, 400,200)
    ctx.stroke()
    ctx.quadraticCurveTo(200,300, 10,60)
    ctx.stroke()
  }
}

function cubicBezier () {
  const ctx = document.querySelector('#cubicBezier').getContext('2d')
  if (!ctx) {
    throw Error
  } else {
    ctx.strokeStyle = 'firebrick'
    ctx.fillStyle = 'salmon'
    ctx.lineWidth = 4

    ctx.moveTo(10, 10)
    ctx.bezierCurveTo(200,10, 50,300, 500,300)
    ctx.lineTo(10, 300)
    ctx.closePath()
    ctx.fill()
    ctx.stroke()
  }
}

quadraticBezier()
cubicBezier()
