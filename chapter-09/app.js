document.addEventListener('DOMContentLoaded', function () {

  const colorOne = '#fc0'
  const colorTwo = '#b4cb02'

  function onDrawRadial () {
    const ctx = document.querySelector('#radial').getContext('2d')
    if (!ctx) {
      throw new Error // => выбросить ошибку, если контекст не был взят
    } else {
      ctx.beginPath()
      ctx.rect(75,100, 250,150)
      
      const radial = ctx.createRadialGradient(150,175,0, 150,175,100)
      radial.addColorStop(0, colorOne)
      radial.addColorStop(1, colorTwo)

      
      ctx.lineWidth = 4
      ctx.stroke()

      ctx.fillStyle = radial
      ctx.fill()
    }
  }

  onDrawRadial()

})

