document.addEventListener('DOMContentLoaded', function () {
  function onDraw () {
    const ctx = document.querySelector('#canvas').getContext('2d')
    if (!ctx) {
      throw new Error // => выбросить ошибку, если контекст не был взят
    } else {

      // FIRST SHAPE
      // -----------------------
      ctx.beginPath() // => open path
      ctx.moveTo(160,130) // start point of new shape
      ctx.lineTo(75,200) // => line
      ctx.lineTo(150,275) // another line
      ctx.lineTo(250,230) // again another line
      ctx.closePath() // => close path

      // make colors
      ctx.strokeStyle = '#333' // => lines color
      ctx.fillStyle = '#fc0' // => fill color
      ctx.lineWidth = 2 // => line width
      
      // fill and stroke commands
      ctx.fill()
      ctx.stroke()

      // SECOND SHAPE
      // ------------------------
      ctx.beginPath() // open path
      ctx.moveTo(50,50) // again move start point of new shape
      ctx.lineTo(450,300) // draw line
      ctx.closePath() // => close path

      ctx.strokeStyle = 'steelblue'
      ctx.lineWidth = 10
      
      
      ctx.stroke() // draw path
    }
  }
  onDraw()
})

