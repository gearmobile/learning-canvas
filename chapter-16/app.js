window.addEventListener('load', function () {

  // => variables
  let xPos = 0
  let radius = 40

  // => get canvas
  const ctx = document.querySelector('#canvas').getContext('2d')

  if (!ctx) {
    throw new Error
  }

  // style canvas
  ctx.fillStyle = 'orange'

  // => get width and height of canvas
  const width = ctx.canvas.width
  const height = ctx.canvas.height

  function animate () {
    if (xPos >= width) {
      xPos = 0
      radius = 40
    } else {
      xPos += 10
      radius += 1

      // => clear canvas
      ctx.clearRect(0,0, width,height)

      // draw circle
      ctx.beginPath()
      ctx.arc(xPos, height / 2, radius, 0, Math.PI * 2, true)
      ctx.fill()
      ctx.closePath()
    }
    
    // => animate canvas
    requestAnimationFrame(animate)
    
  }

  // => call to animate
  requestAnimationFrame(animate)

}, false)


