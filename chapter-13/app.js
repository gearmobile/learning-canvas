document.addEventListener('DOMContentLoaded', function () {

  const colorOne = '#ff6a6a'
  const colorTwo = '#0cf'

  function getRadians (value) {
    if (value === null || value === undefined) {
      throw new Error
    } else {
      return ( Math.PI / 180 ) * value
    }
  }

  function onDrawTranslate () {
    const ctx = document.querySelector('#trans').getContext('2d')
    if (!ctx) {
      throw new Error // => выбросить ошибку, если контекст не был взят
    } else {
      
      // translate and scale
      ctx.translate(50, 50)
      ctx.scale(1.2, 1.2)

      // make circle
      ctx.beginPath()
      ctx.arc(100,100, 90, 0, getRadians(360), true)
      ctx.fillStyle = colorOne
      ctx.fill()
      ctx.closePath()

      // reset translate
      ctx.resetTransform() // => reset all transforms

      // make rectangle
      ctx.beginPath()
      ctx.fillStyle = colorTwo
      ctx.fillRect( 200,100, 180,120 )
      ctx.closePath()
    }
  }

  function onDrawUndo () {
    const ctx = document.querySelector('#undo').getContext('2d')
    if (!ctx) {
      throw new Error // => выбросить ошибку, если контекст не был взят
    } else {
      
      // translate and scale
      ctx.translate( 50, 50 ) // => (1)
      ctx.scale( 1.2, 1.2 ) // => (2)

      // make circle
      ctx.beginPath()
      ctx.arc( 100,100, 90, 0, getRadians(360), true )
      ctx.fillStyle = colorOne
      ctx.fill()
      ctx.closePath()

      // reset translate
      ctx.scale( .6, .6 ) // => (2)
      ctx.translate( -50, -50 ) // => (1)

      // make rectangle
      ctx.beginPath()
      ctx.fillStyle = colorTwo
      ctx.fillRect( 200,100, 180,120 )
      ctx.closePath()
    }
  }

  onDrawTranslate()
  onDrawUndo()

})

