document.addEventListener('DOMContentLoaded', function () {

  const colorOne = '#ff6a6a'
  const colorTwo = '#0cf'
  const message = 'Canvas Rotate'

  function getRadians (value) {
    if (value === null || value === undefined) {
      throw new Error
    } else {
      return ( Math.PI / 180 ) * value
    }
  }

  function onDrawTranslate () {
    const ctx = document.querySelector('#trans').getContext('2d')
    if (!ctx) {
      throw new Error // => выбросить ошибку, если контекст не был взят
    } else {

      // MAKE TRANSLATE
      ctx.translate( 50, 50 ) // => move all canvas right and bottom
      
      // MAKE CIRCLE
      ctx.beginPath()
      ctx.arc( 200,200,90, getRadians(0), getRadians(360), true )
      ctx.fillStyle = colorOne
      ctx.fill()
      ctx.closePath()

      // MAKE SQUARE
      ctx.beginPath()
      ctx.fillStyle = colorTwo
      ctx.fillRect( 50,50, 100,100 )
      ctx.closePath()

    }
  }

  function onDrawRotate (text) {
    const ctx = document.querySelector('#rotate').getContext('2d')
    if (!ctx) {
      throw new Error // => выбросить ошибку, если контекст не был взят
    } else {

      // ROTATE CANVAS
      ctx.rotate(getRadians(15))

      // MAKE RECTANGLE
      ctx.strokeStyle = colorOne
      ctx.lineWidth = 6
      ctx.strokeRect( 100,100, 150,110 )
      ctx.stroke()
    }
  }

  function onDrawScale () {
    const ctx = document.querySelector('#scale').getContext('2d')
    if (!ctx) {
      throw new Error // => выбросить ошибку, если контекст не был взят
    } else {

      // SCALE METHOD
      ctx.scale(2, 1) // => scale up by horizontal axis, remain original scale by vertical axis

      // MAKE RECTANGLE
      ctx.fillStyle = colorTwo
      ctx.lineWidth = 6
      ctx.fillRect( 50,100, 150,110 )
      ctx.fill()
    }
  }

  function onDrawFlip () {
    const ctx = document.querySelector('#flip').getContext('2d')
    if (!ctx) {
      throw new Error // => выбросить ошибку, если контекст не был взят
    } else {

      // FLIP TEXT
      ctx.scale( -0.5, 1 )

      ctx.font = 'bold 60px Roboto, sans-serif'
      ctx.fillText(message, -700,200)
    }
  }

  onDrawTranslate()
  onDrawRotate()
  onDrawScale()
  onDrawFlip()

})

