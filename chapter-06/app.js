document.addEventListener('DOMContentLoaded', function () {

  function radians (value) {
    return (Math.PI / 180) * value
  }

  function onDraw (radius) {
    const ctx = document.querySelector('#canvas').getContext('2d')
    if (!ctx) {
      throw new Error // => выбросить ошибку, если контекст не был взят
    } else {

      ctx.beginPath()
      ctx.arc(ctx.canvas.width / 2, ctx.canvas.height / 2, radius, radians(0), radians(190))
      ctx.closePath()

      ctx.strokeStyle = 'firebrick'
      ctx.lineWidth = 14
      ctx.stroke()
      
      ctx.fillStyle = 'indianred'
      ctx.fill()
    }
  }

  function drawCircle (radius, startAngle, endAngle) {
    const ctx = document.querySelector('#circle').getContext('2d')
    if (!ctx) {
      throw new Error
    } else {
      ctx.beginPath()
      ctx.arc( ctx.canvas.width / 2, ctx.canvas.height / 2, radius, radians(startAngle), radians(endAngle) )
      ctx.closePath()

      ctx.strokeStyle = 'forestgreen'
      ctx.lineWidth = 12
      ctx.stroke()

      ctx.fillStyle = 'lightgreen'
      ctx.fill()
    }
  }

  onDraw(80)
  drawCircle(100,0,360)

})

