'use strict'

document.addEventListener('DOMContentLoaded', function () {

  const circles = {
    amount: 30,
    minRadius: 15,
    maxRadius: 150,
    collection: []
  }

  const circle = {

    // CIRCLE PROPS
    circleID: null,
    circleRadius: null,
    circleXPos: null,
    circleYPos: null,
    circleColor: null,

    // CIRCLE METHODS
    circleInitCenterAndRadius: (x, y) => {
      this.circleXPos = x,
      this.circleYPos = y
      this.circleRadius = Math.round(circles.minRadius + Math.random() * circles.maxRadius)
    },

    circleInitColor: (color) => {
      this.circleColor = color
    },

    circleDraw: (context) => {
      context.beginPath()
      context.arc( this.circleXPos, this.circleYPos, this.circleRadius, 0, Math.PI * 2, true )
      context.fillStyle = this.circleColor
      context.fill()
      context.closePath()
    }
  }

  function getColor() {
    const o = Math.round
    const r = Math.random
    const s = 255
    return 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ',' + r().toFixed(1) + ')'
  }

  function getNewCircle (context, max) {
    for (let i = 0; i < max; i += 1) {

      const xPos = Math.round(Math.random() * context.canvas.width)
      const yPos = Math.round(Math.random() * context.canvas.height)

      const newCircle = Object.create(circle)
      newCircle.circleID = i
      newCircle.circleInitColor(getColor())
      newCircle.circleInitCenterAndRadius(xPos, yPos)
      newCircle.circleDraw(context)
      circles.collection.push(newCircle)
    }
  }

  function onRender () {
    const ctx = document.querySelector('#canvas').getContext('2d')
    if (!ctx) {
      throw new Error // => выбросить ошибку, если контекст не был взят
    } else {
      getNewCircle(ctx, circles.amount)
    }
  }

  onRender()

})

