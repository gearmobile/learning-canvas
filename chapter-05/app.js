document.addEventListener('DOMContentLoaded', function () {

  function onDraw () {
    const ctx = document.querySelector('#canvas').getContext('2d')
    if (!ctx) {
      throw new Error // => выбросить ошибку, если контекст не был взят
    } else {
      ctx.beginPath()
      ctx.moveTo(100,100)
      ctx.lineTo(100,300)
      ctx.lineTo(300,300)
      ctx.closePath()

      ctx.lineWidth = 2
      ctx.strokeStyle = '#666'
      ctx.stroke()

      ctx.fillStyle = '#fc0'
      ctx.fill()
    }
  }

  onDraw()

})

