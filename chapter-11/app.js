document.addEventListener('DOMContentLoaded', function () {

  const message = 'Canvas'

  function onDrawImage () {
    const ctx = document.querySelector('#image').getContext('2d')
    if (!ctx) {
      throw new Error // => выбросить ошибку, если контекст не был взят
    } else {
      const orange = new Image()
      orange.src = './src/orange.svg'
      orange.addEventListener('load', () => { ctx.drawImage(orange, 0,0, 100,100) }, false)
    }
  }

  onDrawImage()

})

