const canvas = document.querySelector('#canvas')
const context = canvas.getContext('2d')

// ---
if (!context) {
  throw new Error // => выбросить ошибку, если контекст не был взят
} else {
  context.moveTo(20, 20)
  context.lineTo(100, 20)
  context.lineTo(250, 40)
  context.lineTo(450, 150)
  context.closePath() // => замкнуть контур

  context.lineWidth = 3 // => толщина линий контура
  context.strokeStyle = 'firebrick' // => цвет линий контура
  context.fillStyle = 'salmon' // => цвет заливки контура

  context.stroke() // => отрисовать линии
  context.fill() // => выполнить заливку замкнутого контура
}