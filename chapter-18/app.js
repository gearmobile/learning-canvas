window.addEventListener('load', function () {

  const ctx = document.querySelector('#canvas').getContext('2d')
  let canvasPosition = updatePosition(ctx.canvas)

  if (!ctx) {
    throw new Error
  }

  // => helper function to return exact position cursor mouse inside some element
  function getElementPosition (el) {
    
    // => init variables
    let xPos = null
    let yPos = null

    if (el) {
      if (el.tagName === 'BODY') {
        const xScroll = el.scrollLeft || document.documentElement.scrollLeft
        const yScroll = el.scrollTop || document.documentElement.scrollTop
        xPos += (el.offsetLeft - xScroll + el.clientLeft)
        yPos += (el.offsetTop - yScroll + el.clientTop) 
      } else {
        xPos += (el.offsetLeft - el.scrollLeft + el.clientLeft)
        yPos += (el.offsetTop - el.scrollTop + el.clientTop)
      }
      el = el.offsetParent
      return { x: xPos, y: yPos }
    }
  }

  function updatePosition () {
    let canvasPosition = getElementPosition(ctx.canvas)
  }

  // => use event mousemove
  ctx.canvas.addEventListener('mousemove', function (event) {
    console.log('---')
  }, false)

  window.addEventListener('scroll', updatePosition, false)

}, false)


