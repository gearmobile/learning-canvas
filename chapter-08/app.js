document.addEventListener('DOMContentLoaded', function () {

  const colorOne = '#ddd'
  const colorTwo = '#2d7bec'
  const colorThree = '#31ffff'

  function onDrawLinear () {
    const ctx = document.querySelector('#linear').getContext('2d')
    if (!ctx) {
      throw new Error // => выбросить ошибку, если контекст не был взят
    } else {
      ctx.beginPath()
      ctx.rect(75, 100, 350, 150)

      const linear = ctx.createLinearGradient( 75,0, 325,0 )
      linear.addColorStop(0.1, colorOne)
      linear.addColorStop(0.75, colorTwo)
      linear.addColorStop(0.9, colorThree)

      ctx.fillStyle = linear
      ctx.fill()

      ctx.lineWidth = 4
      ctx.styleStroke = '#333'
      ctx.stroke()
    }
  }

  function onDrawRadial () {
    const ctx = document.querySelector('#radial').getContext('2d')
    if (!ctx) {
      throw new Error // => выбросить ошибку, если контекст не был взят
    } else {
      //
    }
  }

  onDrawLinear()
  onDrawRadial()

})

