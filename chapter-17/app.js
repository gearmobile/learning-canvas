window.addEventListener('load', function () {

  const ctx = document.querySelector('#canvas').getContext('2d')

  if (!ctx) {
    throw new Error
  }

  // => return screenX and screenY values
  function getCoordsScreen (event) {
    const xPos = event.screenX
    const yPos = event.screenY
    const result = `Mouse screen position is ${xPos} and ${yPos}`
    return result
  }

  // => return clientX and clientY values
  function getCoordsClient (event) {
    const xPos = event.clientX
    const yPos = event.clientY
    const result = `Mouse client position is ${xPos} and ${yPos}`
    return result
  }

  // => return button value
  function getMouseButtons (event) {
    let output = null
    switch (event.button) {
      case 0:
        output = `Left mouse button pressed`
        break
      case 1:
        output = `Middle mouse button pressed`
        break
      case 2:
        output = `Right mouse button pressed`
        break
      default:
        output = `Nothing pressed`
    }
    return output
  }

  // => use event mousemove
  ctx.canvas.addEventListener('mousemove', function (event) {
    console.log('---')
    console.log(getCoordsScreen(event)) // => get screenX and screenY values
    console.log(getCoordsClient(event)) // => get clientX and clientY values
  }, false)

  // => use event mousedown
  ctx.canvas.addEventListener('mousedown', function (event) {
    console.log(getMouseButtons(event)) // => get button value
  }, false)

}, false)


