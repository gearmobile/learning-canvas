document.addEventListener('DOMContentLoaded', function () {

  function onDrawMiter () {
    const ctx = document.querySelector('#miter').getContext('2d')
    if (!ctx) {
      throw new Error // => выбросить ошибку, если контекст не был взят
    } else {
      // lineJoin => miter, round, bevel
      ctx.beginPath()
      ctx.moveTo(100,100)
      ctx.lineTo(100,300)
      ctx.lineTo(300,300)
      ctx.closePath()

      ctx.lineJoin = 'miter'

      ctx.lineWidth = 12
      ctx.stroke()
    }
  }

  function onDrawRound () {
    const ctx = document.querySelector('#round').getContext('2d')
    if (!ctx) {
      throw new Error // => выбросить ошибку, если контекст не был взят
    } else {
      // lineJoin => miter, round, bevel
      ctx.beginPath()
      ctx.moveTo(100, 100)
      ctx.lineTo(100, 300)
      ctx.lineTo(300, 300)
      ctx.closePath()

      ctx.lineJoin = 'round'

      ctx.lineWidth = 12
      ctx.stroke()
    }
  }

  function onDrawBevel () {
    const ctx = document.querySelector('#bevel').getContext('2d')
    if (!ctx) {
      throw new Error // => выбросить ошибку, если контекст не был взят
    } else {
      // lineJoin => miter, round, bevel
      ctx.beginPath()
      ctx.moveTo(100, 100)
      ctx.lineTo(100, 300)
      ctx.lineTo(300, 300)
      ctx.closePath()

      ctx.lineJoin = 'bevel'

      ctx.lineWidth = 12
      ctx.stroke()

      ctx.fillStyle = 'deepskyblue'
      ctx.globalAlpha = 0.6
      ctx.fill()
    }
  }

  onDrawMiter()
  onDrawRound()
  onDrawBevel()

})

