window.addEventListener('load', function () {

  const ctx = document.querySelector('#canvas').getContext('2d')

  if (!ctx) {
    throw new Error
  }

  // helper function convert to radians
  function getRadians (value) {
    return (Math.PI / 180) * value
  }

  // => helper function to return exact position some element
  function getElementPosition (el) {
    
    // => init variables
    let xPos = null
    let yPos = null

    if (el) {
      if (el.tagName === 'BODY') {
        const xScroll = el.scrollLeft || document.documentElement.scrollLeft
        const yScroll = el.scrollTop || document.documentElement.scrollTop
        xPos += (el.offsetLeft - xScroll + el.clientLeft)
        yPos += (el.offsetTop - yScroll + el.clientTop) 
      } else {
        xPos += (el.offsetLeft - el.scrollLeft + el.clientLeft)
        yPos += (el.offsetTop - el.scrollTop + el.clientTop)
      }
      el = el.offsetParent
      return { xPos, yPos }
    }
  }

  const mouse = {
    xPos: null,
    yPos: null
  }

  function update () {
    // => clear canvas
    ctx.clearRect(0,0, ctx.canvas.width, ctx.canvas.height)
    // => draw circle
    ctx.beginPath()
    ctx.arc(mouse.xPos, mouse.yPos, 50, getRadians(0), getRadians(360), true)
    ctx.fillStyle = '#ff6a6a'
    ctx.fill()
    ctx.closePath()
    // => animate circle
    requestAnimationFrame(update)
  }

  // => get mouse cursor position
  function setMousePosition (event) {
    mouse.xPos = event.clientX - getElementPosition(ctx.canvas).xPos // => get current x value cursor mouse inside canvas
    mouse.yPos = event.clientY - getElementPosition(ctx.canvas).yPos // => get current y value cursor mouse inside canvas
  }
  
  ctx.canvas.addEventListener('mousemove', function (event) {
    setMousePosition(event)
  }, false)
  
  update()
}, false)


