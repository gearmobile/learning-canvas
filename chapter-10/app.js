document.addEventListener('DOMContentLoaded', function () {

  const message = 'Canvas'

  function onDrawText () {
    const ctx = document.querySelector('#text').getContext('2d')
    if (!ctx) {
      throw new Error // => выбросить ошибку, если контекст не был взят
    } else {

      ctx.font = '90px Roboto, sans-serif'

      ctx.strokeStyle = 'steelblue'
      ctx.fillStyle = 'forestgreen'
      
      ctx.strokeText(message, 40,125)
      ctx.fillText(message, 40,225)
    }
  }

  onDrawText()

})

